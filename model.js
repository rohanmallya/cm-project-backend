var mongoose = require('mongoose');


var formatJSON = {
    title:String,
    github:String,
    description:String,
    tags:[String]
}

var formatSchema = new mongoose.Schema(formatJSON);
module.exports = mongoose.model('Format', formatSchema);
