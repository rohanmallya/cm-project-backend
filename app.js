var express  = require('express');
var app      = express();
var mongoose = require('mongoose');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cors = require('cors');
var func = require('./functions');
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/cm",{ useNewUrlParser: true });
app.set('view engine','ejs');
app.use(bodyParser.urlencoded({ extended: true })); // Parses urlencoded bodies
app.use(bodyParser.json()); // Send JSON responses
app.use(logger('dev')); // Log requests to API using morgan
app.use(cors());

app.get('/getAll',func.getAll);
app.post('/makeEntry',func.makeEntry);

app.listen(3000, function(err){
    if(err){
        console.log(err);
    }else{
        console.log("Server ON : Port 3000");
    }
})