var Format = require('./model');

exports.getAll = async function(req,res,next){
    var all = await Format.find({});
    res.json(all).status(201);
}

exports.makeEntry = async function(req,res,next){
    console.log("Route: In Make Entry")
    var title = req.body.title;
    var desc = req.body.description;
    var git = req.body.git;
    var tags = req.body.tags;

    var tempJSON = {
        title : title,
        description: desc,
        github: git,
        tags: tags
    }
    console.log(tempJSON);
    var newFormat = new Format(tempJSON);
    await newFormat.save();

}